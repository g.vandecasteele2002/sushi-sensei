import requests
import json

external_base_url = "http://localhost:5001/orders/"


# orderid_example : "9c1172f8-d785-11ec-9725-0242ac120002", time_example: 2022-05-19 16:23:59
def accept_order(order_id: str, time: str):
    body = {"TimeAcceptedFor": time}

    response = requests.put(debug_url + order_id + "/accept", data=json.dumps(body),
                            headers={"Content-Type": "application/json"})
    print(response.text)
    if response.status_code != 200:
        return -1
    else:
        return 0


# orderid_example : "9c1172f8-d785-11ec-9725-0242ac120002"
def reject_order(order_id: str):
    response = requests.put(external_base_url + order_id + "/reject")
    print(response.text)
    if response.status_code != 200:
        return -1
    else:
        return 0


# orderid_example : "9c1172f8-d785-11ec-9725-0242ac120002", time_example: 2022-05-19 16:23:59
def update_duedate(order_id: str, time: str):
    body = {"DueDate": time}

    response = requests.put(external_base_url + order_id + "/duedate", data=json.dumps(body),
                            headers={"Content-Type": "application/json"})
    print(response.text)
    if response.status_code != 200:
        return -1
    else:
        return 0


def update_order_onitsway(order_id: str):
    response = requests.put(external_base_url + order_id + "/deliverystate/onitsway")
    print(response.text)
    if response.status_code != 200:
        return -1
    else:
        return 0


def update_order_delivered(order_id: str):
    response = requests.put(external_base_url + order_id + "/deliverystate/delivered")
    print(response.text)
    if response.status_code != 200:
        return -1
    else:
        return 0
