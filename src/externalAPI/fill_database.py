import datetime
import time
import random
import uuid
import names
import json
from pymongo import MongoClient
from pprint import pprint

# list for the final document with everything embedded
order_containers: list[dict] = []

sushis: list[dict] = [json.loads(
    '{"name": "Green and Red Ura Maki Roll", "description": "vegetarian", "price": 1090, "notes": "user entered no notes"}'),
    json.loads(
        '{"name": "Mr Crabs Ura Maki Roll", "description": "with fish", "price": 1090, "notes": "user entered no notes"}'),
    json.loads(
        '{"name": "Crunchy Futo Maki Ebi Tempura","description": "with fish","price": 1290,"notes": "user entered no notes"}'),
    json.loads(
        '{"name": "Sunny Hawaii Tempure Ura Maki","description": "with fish","price": 1390,"notes": "user entered no notes"}')]

streets = ["Kapuzinergasse", "Maximilianstraße", "Imhofstrasse", "Frauentorstrasse", "Ludwigstrasse",
           "Neuhäuserstrasse", "Halderstrasse", "Unterer Graben", "Fuggerstrasse", "Frölichstraße"]

while len(order_containers) < 10:

    order = {"Id": str(uuid.uuid1()), "created_at": "not set", "deliver_at": "not set"}

    # Order's person creation
    delivery = {
        "first_name": names.get_first_name(),
        "last_name": names.get_last_name(),
        "phone_number": str(random.randint(1000000000, 9999999999)),
        "street": random.choice(streets),
        "street_number": str(random.randint(1, 999)),
        "postcode": "86150"
    }
    order["delivery"] = delivery

    # Order's sushis creation
    items = []
    for i in range(random.randint(1, 4)):
        items.append(sushis[random.randint(0, len(sushis)) - 1])

    order["items"] = items

    # order container creation
    order_container = {"state": "not sent"}
    order_container["order"] = order
    order_containers.append(order_container)

client = MongoClient("mongodb://localhost/")
db = client.test

for mydocument in order_containers:
    result = db.order_containers.insert_one(mydocument)
