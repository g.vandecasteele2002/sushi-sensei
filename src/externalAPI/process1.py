import datetime
import random
import requests
from pymongo import MongoClient
import time


def process1(db, fixed_interval=0):
    if fixed_interval == 0:
        while True:
            wait_time = random.randint(0, 60)
            print("sending next request in ", wait_time, " seconds... \n")
            time.sleep(wait_time)
            new_order_request(db)
            print("request sent! \n")

    else:
        wait_time = fixed_interval
        while True:
            print("sending next request in ", wait_time, " seconds... \n")
            time.sleep(wait_time)
            new_order_request(db)
            print("request sent! \n")


def new_order_request(db):
    # get next suitable request from database
    result = db.order_containers.find({"state": "not sent"}).limit(1)

    order = result.next()

    date_format_str = '%d/%m/%Y %H:%M:%S'
    now = datetime.datetime.now()
    created_at = now.strftime("%Y-%m-%d %H:%M:%S")
    delivered_at = (now + datetime.timedelta(minutes=random.randint(40, 90))).strftime("%Y-%m-%d %H:%M:%S")

    order["order"]["created_at"] = created_at
    order["order"]["deliver_at"] = delivered_at

    # send inner order object here
    response = requests.post("https://34d4ab28-cf7c-4fea-8335-de2d9b8fd022.mock.pstmn.io/order", json=order["order"],
                             headers={"Authorization": "my_token"})

    print(response)
    order["state"] = "sent"

    # write back to database here
    res = db.order_containers.update_one({"_id": order["_id"]}, {"$set": order}, upsert=True)


client = MongoClient("mongodb://localhost:27017")
database = client.justeat

process1(database, fixed_interval=20)
