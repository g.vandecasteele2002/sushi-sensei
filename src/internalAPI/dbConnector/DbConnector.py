import datetime
import json

import pymysql
from pymysql import connections

from src.internalAPI.businessObjects.Delivery import Delivery
from src.internalAPI.businessObjects.Item import Item
from src.internalAPI.businessObjects.Order import Order
from src.internalAPI.businessObjects.OrderDetail import OrderDetail


class DbConnector:
    """

    """
    con: connections.Connection = None

    def __new__(cls):
        """ creates a singleton object, if it is not created,
        or else returns the previous singleton object"""
        if not hasattr(cls, 'instance'):
            cls.instance = super(DbConnector, cls).__new__(cls)
        return cls.instance

    def connect(self) -> connections.Connection:
        self.con = pymysql.connect(host='localhost', user='root', password='ebisuforever123', database='ebisu',
                                   port=3306)
        return self.con


def post_order(order: Order):
    crs = DbConnector().connect().cursor()
    try:

        delivery = order.delivery
        order_details = order.order_details

        query_delivery = "INSERT INTO `delivery` (`delivery_id`,`first_name`,`last_name`,`phone_number`, `street`, " \
                         "`house_number`, `postcode`) VALUES(%s,%s,%s,%s,%s,%s)"
        crs.execute(query_delivery, (
            delivery.first_name, delivery.last_name, delivery.phone_number, delivery.phone_number, delivery.street,
            delivery.house_number, delivery.postcode))

        query_order = "INSERT INTO `orders` (`order_id`,`created_at`,`deliver_at`, `delivery_id`," \
                      "`delivery_time_adjusted`) VALUES(%s,%s,%Y-%m-%d %H:%M:%S,%Y-%m-%d %H:%M:%S, %s,%s)"
        crs.execute(query_order,
                    (order.id, order.created_at, order.deliver_at, delivery.id, order.delivery_time_adjusted))

        for order_detail in order_details:
            item = order_detail.item
            item_query = "INSERT INTO `item` (`idem_id`,`name`,`description`,`price`,`instructions`, " \
                         "`fried_roll`, `fried_ingredient`, `friend_ingredient_count`) VALUES(%s,%s,%s,%d,%s,%d,%s,%d)"
            crs.execute(item_query,
                        (item.id, item.name, item.description, item.price, item.instructions, item.fried_roll,
                         item.fried_ingredient, item.friend_ingredient_count))

            order_detail_query = "INSERT INTO `order_detail` (`order_detail_id`,`order_id`,`quantity`,`notes`, " \
                                 "`item_id`, `station`) VALUES(%s,%s,%d,%s, %s,%s)"
            crs.execute(order_detail_query,
                        (order_detail.id, order.id, order_detail.quantity, order_detail.notes, item.id,
                         order_detail.station))

        DbConnector.con.commit()

    except:
        print("Request to MySQL DB incomplete, src not implemented [post_order]")


def get_orders():
    crs = DbConnector().connect().cursor()

    query = "SELECT * FROM `orders`"
    crs.execute(query)
    result_set = []

    for (order_id, created_at, deliver_at, delivery_id, delivery_time_adjusted) in crs.fetchall():
        delivery = get_delivery_by_id(delivery_id)
        order_details = get_order_details_by_order_id(order_id)

        order = Order(uid=order_id, created_at=created_at, deliver_at=deliver_at,
                      delivery_time_adjusted=delivery_time_adjusted, delivery=delivery, order_details=order_details)
        result_set.append(order)

    return result_set


def get_delivery_by_id(delivery_id: str) -> Delivery:
    crs = DbConnector().connect().cursor()
    query = "SELECT * FROM `delivery` WHERE `delivery_id`=%s"

    crs.execute(query, delivery_id)
    delivery_result = crs.fetchone()

    return Delivery(uid=delivery_result[0], first_name=delivery_result[1], last_name=delivery_result[2],
                    phone_number=delivery_result[3], street=delivery_result[4], house_number=delivery_result[5],
                    postcode=delivery_result[6])


def get_order_details_by_order_id(order_id) -> [OrderDetail]:
    crs = DbConnector().connect().cursor()
    query = "SELECT * FROM `order_detail` WHERE `order_id`=%s"
    crs.execute(query, order_id)

    order_details = []
    for (order_details_id, order_id, quantity, notes, item_id, station) in crs.fetchall():
        item = get_item_by_id(item_id)
        new_order_detail = OrderDetail(uid=order_details_id, order_id=order_id, quantity=quantity,
                                       notes=notes, item=item, station=station)
        order_details.append(new_order_detail)

    return order_details


def get_order_details_by_order_detail_id(order_detail_id) -> [OrderDetail]:
    crs = DbConnector().connect().cursor()
    query = "SELECT * FROM `order_detail` WHERE `order_detail_id`=%s"
    crs.execute(query, order_detail_id)

    result = crs.fetchone()
    new_order_detail = OrderDetail(uid=result[0], order_id=result[1], quantity=result[2],
                                   notes=result[3], item=get_item_by_id(result[4]), station=result[5])

    return new_order_detail


def get_item_by_id(item_id):
    crs = DbConnector().connect().cursor()
    query = "SELECT * FROM `item` WHERE `item_id`=%s"

    crs.execute(query, item_id)
    item = crs.fetchone()

    result_item = Item(uid=item[0], name=item[1], description=item[2], price=item[3], instructions=item[4],
                       fried_roll=item[5], fried_ingredient=item[6], fried_ingredient_count=item[7])

    return result_item


def update_station_in_order_details(order_details: OrderDetail):
    conn = DbConnector().connect()
    crs = conn.cursor()
    query = "UPDATE `order_detail` SET `station`=%s WHERE `order_detail_id`=%s"
    crs.execute(query, (order_details.station, order_details.id))
    crs.close()
    conn.commit()


def update_deliver_at_in_order(order_id: str, minutes_added: int):
    conn = DbConnector().connect()
    crs = conn.cursor()

    # TODO: Write and execute UPDATE query for datetime (including format transformation and timestamp generation)

    crs.close()
    conn.commit()


def get_orders_by_orders_details_ids(orders_details_ids: [str]) -> [Order]:
    crs = DbConnector().connect().cursor()
    order_ids = []
    for order_detail_id in orders_details_ids:
        query = "SELECT `order_id` FROM `order_detail` WHERE `order_detail_id`=%s"
        crs.execute(query, order_detail_id)
        order_ids.append(crs.fetchone())

    orders = []
    for order_id in order_ids:
        query = "SELECT * FROM `order` WHERE `order_id`=%s"
        crs.execute(query, order_id)
        for (order_id, created_at, deliver_at, delivery_id, delivery_time_adjusted) in crs.fetchall():
            delivery = get_delivery_by_id(delivery_id)
            order_details = get_order_details_by_order_id(order_id)
            new_order = Order(uid=order_id, order_details=order_details, created_at=created_at,
                              deliver_at=deliver_at, delivery=delivery, delivery_time_adjusted=delivery_time_adjusted)
            orders.append(new_order)

    return orders
