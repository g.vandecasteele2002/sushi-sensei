import uuid

from src.internalAPI.businessObjects import Item


class PrioritizedOrderDetail:

    def __init__(self, order_id: str, quantity, item: Item, priority: int, notes="", station="REGISTER",
                 uid=uuid.uuid1()):
        self.id = uid
        self.order_id = order_id
        self.quantity = quantity
        self.notes = notes
        self.item = item
        self.station = station
        self.priority = priority
