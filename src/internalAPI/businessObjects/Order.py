import uuid

from src.internalAPI.businessObjects import Delivery, OrderDetail


class Order:

    def __init__(self, order_details: list[OrderDetail], created_at, deliver_at, delivery: Delivery, uid=uuid.uuid1(),
                 delivery_time_adjusted=False):
        self.id = uid
        self.order_details: list[OrderDetail] = order_details
        self.created_at = created_at
        self.deliver_at = deliver_at
        self.delivery = delivery
        self.delivery_time_adjusted = delivery_time_adjusted
