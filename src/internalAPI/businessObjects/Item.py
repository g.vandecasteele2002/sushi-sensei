import uuid


class Item:

    def __init__(self, name, description, price: float, instructions, fried_roll: bool, fried_ingredient,
                 fried_ingredient_count, uid=uuid.uuid1()):
        self.id = uid
        self.name = name
        self.description = description
        self.price = price
        self.instructions = instructions
        self.fried_roll = fried_roll
        self.fried_ingredient = fried_ingredient
        self.fried_ingredient_count = fried_ingredient_count
