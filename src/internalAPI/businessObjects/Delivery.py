import uuid


class Delivery:

    def __init__(self, first_name, last_name, phone_number, street, house_number, postcode, uid=uuid.uuid1()):
        self.id = uid
        self.first_name = first_name
        self.last_name = last_name
        self.phone_number = phone_number
        self.street = street
        self.house_number = house_number
        self.postcode = postcode
