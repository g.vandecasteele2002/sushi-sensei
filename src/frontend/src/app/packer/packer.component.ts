import { Component, OnInit } from '@angular/core';
import { RetrieveService } from '../retrieve.service';
import { order, item, order_detail, delivery, front_item } from '../retrieve.service';


@Component({
  selector: 'app-packer',
  templateUrl: './packer.component.html',
  styleUrls: ['./packer.component.css']
})
export class PackerComponent implements OnInit {

  constructor(private retrieveService : RetrieveService) { }

  interval: any;

    ngOnInit() {
        this.refresh();
        this.interval = setInterval(() => { 
            this.refresh(); 
        }, 5000);
    }

    ngOnDestroy() {
        clearInterval(this.interval);
    }

  title = 'sushi';
  current_description = "Please select an item"

  moved_items =  new Array<front_item>(); 

  
  
  mark_moved(my_item : front_item) {
    this.moved_items.push(my_item);
  }

  

  is_moved(my_item : front_item) {
      return (this.moved_items.filter(i => i == my_item).length > 0)
        
  }


  packed_items =  new Array<front_item>(); 

  change_packed(my_item : front_item) {
    if (this.is_packed(my_item)) {
      this.unpack(my_item)
    }
    else {
      this.pack(my_item)
    }
  }

  
  pack(my_item : front_item) {
    this.packed_items.push(my_item);
  }

  unpack(my_item : front_item) {
    this.packed_items.splice(this.packed_items.indexOf(my_item),1)
  }

  is_packed(my_item : front_item) {
      return (this.packed_items.filter(i => i == my_item).length > 0)
        
  }

  is_fried(my_item : front_item) {
    return my_item.fried_roll
  }

  

  backward(my_item : front_item) {
    this.mark_moved(my_item)
    this.retrieveService.movePreviousStation(my_item.order_detail_id)
  }

  show_details(my_item : front_item) {
    this.current_description = my_item.item.instructions
  }

 pack_order(order : order) {
  this.retrieveService.moveWholeOrderToDeliveryDriver(order.Id) 
  
  //remove the items from that order frome the packed_orders
  this.packed_items = this.packed_items.filter(it => it.order_id != order.Id)    
  
 }

 

 is_all_at_packer_station(order : order) {
    
    return !(order.order_details.filter( (od : any) => od.station !== this.my_station).length > 0)
    

 }

 get_package_information(order : order) {
   return this.retrieveService.getPackageInformation(order.Id)
 }

  my_station = "PACKER"

  my_front_items =  new Array<front_item>();
  my_orders = new Array<order>();

  get_items_by_order(order : order) {
    return this.my_front_items.filter(fi => fi.order_id == order.Id)
  }

  refresh() {
    
    console.log("refreshing data...")
    let collect_my_orders = new Array<order>();
    collect_my_orders = this.retrieveService.getOrdersForStation(this.my_station)

    setTimeout(() => {  
    
    let collect_items = new Array<front_item>();
    console.log(this.my_orders);
    console.log(collect_my_orders);
    collect_my_orders.forEach(function(obj1) 
    { obj1.order_details.forEach(function(obj2 : order_detail) 
      { collect_items.push({"order_id": obj1.Id, "order_detail_id": obj2.order_detail_id, "item" : obj2.item, "deliver_at": obj1.deliver_at, "delivery": obj1.delivery, "station": obj2.station, "priority": obj2.priority, "fried_roll": obj2.item.fried_roll, "fried_ingredient": obj2.item.fried_ingredient, "fried_ingredient_count": obj2.item.fried_ingredient_count, "quantity": obj2.quantity})
    }
    ); 
    })
    
    collect_items.sort((a,b) => a.priority - b.priority)
    this.my_orders = collect_my_orders
    this.my_front_items = collect_items
  
  }, 2000);

  }




}
