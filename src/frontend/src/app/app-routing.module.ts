import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { DelivererComponent } from './deliverer/deliverer.component';
import { FrierComponent } from './frier/frier.component';
import { HomeComponent } from './home/home.component';
import { KasseComponent } from './kasse/kasse.component';
import { PackerComponent } from './packer/packer.component';
import { SushiMakerComponent } from './sushi-maker/sushi-maker.component';

const routes: Routes = [
  { path: 'kasse', component: KasseComponent },
  { path: 'sushi-maker', component: SushiMakerComponent },
  { path: 'frier', component: FrierComponent },
  { path: 'packer', component: PackerComponent },
  { path: 'deliverer', component: DelivererComponent },
  { path: 'home', component: HomeComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full'},
  { path: '**', redirectTo: '/home'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
