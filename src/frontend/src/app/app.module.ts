import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { KasseComponent } from './kasse/kasse.component';
import { FrierComponent } from './frier/frier.component';
import { PackerComponent } from './packer/packer.component';
import { DelivererComponent } from './deliverer/deliverer.component';
import { SushiMakerComponent } from './sushi-maker/sushi-maker.component';
import { HomeComponent } from './home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatListModule } from '@angular/material/list';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    KasseComponent,
    FrierComponent,
    PackerComponent,
    DelivererComponent,
    SushiMakerComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatListModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
