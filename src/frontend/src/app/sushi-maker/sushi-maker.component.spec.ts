import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SushiMakerComponent } from './sushi-maker.component';

describe('SushiMakerComponent', () => {
  let component: SushiMakerComponent;
  let fixture: ComponentFixture<SushiMakerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SushiMakerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SushiMakerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
