import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FrierComponent } from './frier.component';

describe('FrierComponent', () => {
  let component: FrierComponent;
  let fixture: ComponentFixture<FrierComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FrierComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FrierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
