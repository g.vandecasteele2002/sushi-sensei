import { Component, OnInit } from '@angular/core';
import { RetrieveService } from '../retrieve.service';
import { order, item, order_detail, delivery, front_item } from '../retrieve.service';

@Component({
  selector: 'app-frier',
  templateUrl: './frier.component.html',
  styleUrls: ['./frier.component.css']
})
export class FrierComponent implements OnInit {

  constructor(private retrieveService : RetrieveService) {

  }

  interval: any;

    ngOnInit() {
        this.refresh();
        this.interval = setInterval(() => { 
            this.refresh(); 
        }, 5000);
    }

    ngOnDestroy() {
        clearInterval(this.interval);
    }
  

  title = 'sushi';
  current_description = "Please select an item"
  current_fried_information = ""

  moved_items =  new Array<front_item>(); 

  
  
  mark_moved(my_item : front_item) {
    this.moved_items.push(my_item);
  }

  

  is_moved(my_item : front_item) {
      return (this.moved_items.filter(i => i == my_item).length > 0)
        
  }



  forward(my_item : front_item) {
    this.mark_moved(my_item)
    this.retrieveService.moveNextStation(my_item.order_detail_id)
  }

  backward(my_item : front_item) {
    this.mark_moved(my_item)
    this.retrieveService.movePreviousStation(my_item.order_detail_id)
  }

  show_details(my_item : front_item) {
    this.current_description = my_item.item.instructions
    this.current_fried_information = ""
    if (my_item.fried_roll) {
      this.current_fried_information = "fried ingredient: " +  my_item.fried_ingredient_count + " " +  my_item.fried_ingredient
    }
  }

  get_items_by_order(order : order) {
    
    return this.my_front_items.filter(fi => fi.order_id == order.Id)
  }

  my_station = "FRYER"

  my_front_items =  new Array<front_item>();
  my_orders = new Array<order>();

  refresh() {
    
    console.log("refreshing data...")
    let collect_my_orders = new Array<order>();
    collect_my_orders = this.retrieveService.getOrdersForStation(this.my_station)

    setTimeout(() => {  
    
    let collect_items = new Array<front_item>();
    console.log(this.my_orders);
    console.log(collect_my_orders);
    collect_my_orders.forEach(function(obj1) 
    { obj1.order_details.forEach(function(obj2 : order_detail) 
      { collect_items.push({"order_id": obj1.Id, "order_detail_id": obj2.order_detail_id, "item" : obj2.item, "deliver_at": obj1.deliver_at, "delivery": obj1.delivery, "station": obj2.station, "priority": obj2.priority, "fried_roll": obj2.item.fried_roll, "fried_ingredient": obj2.item.fried_ingredient, "fried_ingredient_count": obj2.item.fried_ingredient_count, "quantity": obj2.quantity})
    }
    ); 
    })
    
    collect_items.sort((a,b) => a.priority - b.priority)
    this.my_orders = collect_my_orders
    this.my_front_items = collect_items
  
  }, 2000);

  }

}