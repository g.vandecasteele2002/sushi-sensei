import { Component, OnInit } from '@angular/core';
import { RetrieveService } from '../retrieve.service';
import { order, item, order_detail, delivery, front_item } from '../retrieve.service';

@Component({
  selector: 'app-deliverer',
  templateUrl: './deliverer.component.html',
  styleUrls: ['./deliverer.component.css']
})
export class DelivererComponent implements OnInit {

  constructor(private retrieveService : RetrieveService) { }

  interval: any;

    ngOnInit() {
        this.refresh();
        this.interval = setInterval(() => { 
            this.refresh(); 
        }, 5000);
    }

    ngOnDestroy() {
        clearInterval(this.interval);
    }

  title = 'sushi';
  current_description = "Please select an item"

  

  onitsway_orders =  new Array<order>(); 
  

 

  
  push_onitsway(my_order : order) {
    this.onitsway_orders.push(my_order);
  }

  clear_onitsway(my_order : order) {
    this.onitsway_orders.splice(this.onitsway_orders.indexOf(my_order),1)
  }

  is_onitsway(my_order : order) {
      return (this.onitsway_orders.filter(i => i == my_order).length > 0)
        
  }

  show_details(my_item : front_item) {
    this.current_description = my_item.item.instructions
  }

  reject_whole_order(order : order) {
    if (this.is_onitsway(order)) {
      this.clear_onitsway(order)
    }
    
    this.retrieveService.moveBackToPacker(order.Id) 
  }

  change_state_onitsway(order : order) {
    this.retrieveService.updateStatus_onitsway(order.Id)
    this.push_onitsway(order)
  }

  change_state_deliverd(order : order) {
    this.retrieveService.updateStatus_delivered(order.Id)
    this.clear_onitsway(order)
  }

  get_items_by_order(order : order) {
    return this.my_front_items.filter(fi => fi.order_id == order.Id)
  }
  

  my_station = "DELIVERY_DRIVER"

  my_front_items =  new Array<front_item>();
  my_orders = new Array();

  refresh() {
    
    console.log("refreshing data...")
    let collect_my_orders = new Array<order>();
    collect_my_orders = this.retrieveService.getOrdersForStation(this.my_station)

    setTimeout(() => {  
    
    let collect_items = new Array<front_item>();
    console.log(this.my_orders);
    console.log(collect_my_orders);
    collect_my_orders.forEach(function(obj1) 
    { obj1.order_details.forEach(function(obj2 : order_detail) 
      { collect_items.push({"order_id": obj1.Id, "order_detail_id": obj2.order_detail_id, "item" : obj2.item, "deliver_at": obj1.deliver_at, "delivery": obj1.delivery, "station": obj2.station, "priority": obj2.priority, "fried_roll": obj2.item.fried_roll, "fried_ingredient": obj2.item.fried_ingredient, "fried_ingredient_count": obj2.item.fried_ingredient_count, "quantity": obj2.quantity})
    }
    ); 
    })
    
    collect_items.sort((a,b) => a.priority - b.priority)
    this.my_orders = collect_my_orders
    this.my_front_items = collect_items
  
  }, 2000);

  }



}
