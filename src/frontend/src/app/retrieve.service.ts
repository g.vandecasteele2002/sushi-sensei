export interface item {
   Id: string,
   name: string,
   description: string,
   price: number,
   instructions: string,
   fried_roll: boolean,
   fried_ingredient: string,
   fried_ingredient_count: number
 }

 export interface order_detail {
  order_detail_id: string,
  order_id: string,
  quantity: number,
  notes: string,
  item: item,
  station: string,
  priority: number
 }

 export interface delivery {
   Id: string,
   first_name: string,
   last_name: string,
   street: string,
   house_number: string,
   postcode: string
   phone_number: string
 }

export interface order {
  Id: string,
  order_details: order_detail[],
  created_at: string,
  deliver_at: string,
  delivery: delivery,
  delivery_time_adjusted: number
}

export interface front_item  {
  order_id: string,
  order_detail_id: string,
  item: item,
  deliver_at: string,
  delivery: delivery,
  station: string,
  priority: number,
  fried_roll: boolean,
  fried_ingredient_count: number,
  fried_ingredient: string,
  quantity: number
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})

export class RetrieveService {

  constructor(private http: HttpClient) {}

    
myTypedMockOrders = new Array<order>();


base_url = "http://localhost:5000/"
base_url_debug = "http://localhost:3000/"

getOrdersForStation(station : string) {
  
  this.myTypedMockOrders = [] as order[]
  this.http.get(this.base_url + "prioritized_orders/" + station).subscribe((x) => {
    console.log(x);
    (<order[]>x).forEach((y) => {
          
          
          this.myTypedMockOrders.push(y)
          
        }
    )})

    
  return this.myTypedMockOrders;
  
}

moveNextStation(my_order_detail_id : string) {
  this.http.put(this.base_url + "move_next_station/" + my_order_detail_id, "").subscribe()
  console.log("moving item " + my_order_detail_id + " to next station.")
}

movePreviousStation(my_order_detail_id : string) {
  this.http.put(this.base_url + "move_previous_station/" + my_order_detail_id, "").subscribe()
  console.log("moving item " + my_order_detail_id + " to previous station.")
}

moveWholeOrderToDeliveryDriver(my_order_id : string) {
  this.http.put(this.base_url + "move_to_delivery_driver/" + my_order_id, "").subscribe()
  console.log("moving whole order " + my_order_id + " from packer to delivery driver.")
}

getPackageInformation(my_order_id : string) : string {
  console.log("requesting package information for order " + my_order_id)
  return "Large. Squared"
}

updateStatus_onitsway(my_order_id : any) {
  this.http.put(this.base_url + "mark_as_onitsway/" + my_order_id, "").subscribe()
  console.log("updating status for order " + my_order_id + " to onitsway")
}

updateStatus_delivered(my_order_id : any) {
  this.http.put(this.base_url + "mark_asdelivered/" + my_order_id, "").subscribe()
  console.log("updating status for order " + my_order_id + " to  delivered")
}

confirmOrder(my_order_id : string, time : number) {
  this.http.put(this.base_url + "confirm_order/" + my_order_id + "/" + time, "").subscribe()
}

moveBackToPacker(my_order_id : string) {
  this.http.put(this.base_url + "move_back_to_packer/" + my_order_id, "").subscribe()
}






}

