from flask import Flask
from flask_cors import CORS
from flask_restx import Api, Resource, fields

from internalAPI.businessObjects.Item import Item
from internalAPI.businessObjects.Order import Order
from internalAPI.businessObjects.OrderDetail import OrderDetail
from internalAPI.businessObjects.PrioritizedOrder import PrioritizedOrder
from internalAPI.businessObjects.PrioritizedOrderDetail import PrioritizedOrderDetail
from internalAPI.dbConnector import DbConnector

app = Flask(__name__)
ebisu_api = Api(app)
cors = CORS(app, resources={r"/ebisu_api/*": {"origins": "*"}})

delivery = ebisu_api.model('Delivery', {
    'Id': fields.String(required=False, attribute='id', description='UID of the delivery'),
    'first_name': fields.String(required=True, attribute='first_name', description='First name of the delivery'),
    'last_name': fields.String(required=True, attribute='last_name', description='Last name of the delivery'),
    'phone_number': fields.String(required=True, attribute='phone_number', description='Phone number of the delivery'),
    'street': fields.String(required=True, attribute='street', description='Street of the delivery'),
    'house_number': fields.String(required=True, attribute='house_number', description='House number of the delivery'),
    'postcode': fields.String(required=True, attribute='postcode', description='Postcode of the delivery')
})

item = ebisu_api.model('Item', {
    'Id': fields.String(required=False, attribute='id', description='UID of the order'),
    'name': fields.String(required=True, attribute='name', description='name of the item'),
    'description': fields.String(required=True, attribute='description', description='description of the item'),
    'price': fields.Integer(required=True, attribute='price', description='price of the item in ct.'),
    'instructions': fields.String(required=False, attribute='instructions',
                                  description='instructions to make the roll'),
    'fried_roll': fields.Boolean(required=True, attribute='fried_roll',
                                 description='describes whether the roll itself is fried'),
    'fried_ingredient': fields.String(required=False, attribute='fried_ingredient',
                                      description='name of the fried ingredients in the roll, set to NULL, '
                                                  'if the roll does not contain any'),
    'fried_ingredient_count': fields.Integer(required=False, attribute='fried_ingredient_count',
                                             description='number of fried ingredients')
})

order_detail = ebisu_api.model('OrderDetail', {
    'order_detail_id': fields.String(required=False, attribute='id', description='UID of the order detail'),
    'order_id': fields.String(required=True, attribute='order_id', description='UID of the order'),
    'quantity': fields.Integer(required=True, attribute='quantity', description="Item's quantity"),
    'notes': fields.String(required=False, attribute='notes', description="Notes for the detail"),
    'item': fields.Nested(item, required=True, attribute='item', description="item object"),
    'station': fields.String(required=True, attribute='station', description="Station for the item")
})

order = ebisu_api.model('Order', {
    'Id': fields.String(required=False, attribute='id', description='UID of the order'),
    'order_details': fields.List(required=True, attribute='order_details',
                                 description='Order details contained in the order',
                                 cls_or_instance=fields.Nested(order_detail)),
    'created_at': fields.DateTime(required=True, attribute='created_at', description='Timestamp of order creation'),
    'deliver_at': fields.DateTime(required=True, attribute='deliver_at',
                                  description='Timestamp of supposed order delivery'),
    'delivery': fields.Nested(delivery, required=True, attribute='delivery',
                              description='Timestamp of supposed order delivery'),
    'delivery_time_adjusted': fields.Integer(required=False, attribute='delivery_time_adjusted',
                                             description='Delivery time of the order')
})

prioritized_order_detail = ebisu_api.model('PrioritizedOrderDetail', {
    'order_detail_id': fields.String(required=False, attribute='id', description='UID of the order detail'),
    'order_id': fields.String(required=True, attribute='order_id', description='UID of the order'),
    'quantity': fields.Integer(required=True, attribute='quantity', description="Item's quantity"),
    'notes': fields.String(required=False, attribute='notes', description="Notes for the detail"),
    'item': fields.Nested(item, required=True, attribute='item', description="item object"),
    'station': fields.String(required=True, attribute='station', description="Station for the item"),
    'priority': fields.Integer(required=True, attribute='priority', description="Index of priority inside the order")
})

prioritized_order = ebisu_api.model('PrioritizedOrder', {
    'Id': fields.String(required=False, attribute='id', description='UID of the order'),
    'order_details': fields.List(required=True, attribute='order_details',
                                 description='Order details contained in the order',
                                 cls_or_instance=fields.Nested(prioritized_order_detail)),
    'created_at': fields.DateTime(required=True, attribute='created_at', description='Timestamp of order creation'),
    'deliver_at': fields.DateTime(required=True, attribute='deliver_at',
                                  description='Timestamp of supposed order delivery'),
    'delivery': fields.Nested(delivery, required=True, attribute='delivery',
                              description='Timestamp of supposed order delivery'),
    'delivery_time_adjusted': fields.Integer(required=False, attribute='delivery_time_adjusted',
                                             description='Delivery time of the order')
})


@ebisu_api.route("/orders")
class Orders(Resource):

    @ebisu_api.marshal_with(order)
    @ebisu_api.expect(order)
    def post(self):
        DbConnector.post_order(order)
        return Order, 200, {'Access-Control-Allow-Origin': '*'}

    @ebisu_api.marshal_with(order)
    def get(self):
        return DbConnector.get_orders(), 200, {'Access-Control-Allow-Origin': '*'}


@ebisu_api.route("/orders/<string:station>")
class OrdersByStation(Resource):

    @ebisu_api.marshal_with(order)
    def get(self, station):
        result_set = []

        for entry in DbConnector.get_orders():
            for entry_order_detail in entry.order_details:
                if entry_order_detail.station == station and entry not in result_set:
                    result_set.append(entry)

        return result_set, 200, {'Access-Control-Allow-Origin': '*'}


@ebisu_api.route("/prioritized_orders/<string:station>")
class PrioritizedOrdersByStation(Resource):

    @ebisu_api.marshal_with(prioritized_order)
    def get(self, station):
        # Getting orders filtered by the station passed in the function argument
        orders_by_station = []
        for entry in DbConnector.get_orders():
            for entry_order_detail in entry.order_details:
                if entry_order_detail.station == station and entry not in orders_by_station:
                    orders_by_station.append(entry)

        # Iterating through the orders based on the index (so we have an index to use for
        # the prioritization of the orders
        prioritized_orders_by_station = []
        for order in orders_by_station:
            order_details = []

            # Turning OrderDetail objects into PrioritizedOrderDetail objects
            for i in range(len(order.order_details)):
                order_detail = order.order_details[i]
                prio_order_detail = PrioritizedOrderDetail(uid=order_detail.id, order_id=order_detail.order_id,
                                                           quantity=order_detail.quantity, notes=order_detail.notes,
                                                           item=order_detail.item, station=order_detail.station,
                                                           priority=i + 1)
                order_details.append(prio_order_detail)

            # Creating a PrioritizedOrder objects based on the previously created  PrioritizedOrderDetail objects
            prio_order = PrioritizedOrder(uid=order.id, created_at=order.created_at, deliver_at=order.deliver_at,
                                          delivery_time_adjusted=order.delivery_time_adjusted, delivery=order.delivery,
                                          order_details=order_details)
            prioritized_orders_by_station.append(prio_order)

        return prioritized_orders_by_station, 200, {'Access-Control-Allow-Origin': '*'}


NRRI = ["REGISTER", "SUSHI_MAKER", "PACKER", "DELIVERY_DRIVER", "DELIVERED"]
NRFI = ["REGISTER", "SUSHI_MAKER", "PACKER", "DELIVERY_DRIVER", "DELIVERED"]
FRFI = ["REGISTER", "SUSHI_MAKER", "FRYER", "PACKER", "DELIVERY_DRIVER", "DELIVERED"]
FRRI = ["REGISTER", "SUSHI_MAKER", "FRYER", "PACKER", "DELIVERY_DRIVER", "DELIVERED"]


@ebisu_api.route("/confirm_order/<string:order_id>/<int:time_added>")
class ConfirmOrderAtRegister(Resource):

    @ebisu_api.marshal_with(order)
    def put(self, order_id, time_added):
        found_order: Order = None

        for order in DbConnector.get_orders():
            if order.id == order_id:
                found_order = order

        DbConnector.update_deliver_at_in_order(found_order.id, time_added)

        for order_detail in found_order.order_details:
            MoveToNextStation().put(order_detail.id)

        return None, 200, {'Access-Control-Allow-Origin': '*'}


@ebisu_api.route("/move_next_station/<string:order_detail_id>")
class MoveToNextStation(Resource):

    @ebisu_api.marshal_with(order)
    def put(self, order_detail_id):
        order_details: OrderDetail = DbConnector.get_order_details_by_order_detail_id(order_detail_id)
        item = order_details.item

        station_sequence = find_station_sequence(item)
        next_station = station_sequence[station_sequence.index(order_details.station) + 1]
        order_details.station = next_station

        DbConnector.update_station_in_order_details(order_details)

        return None, 200, {'Access-Control-Allow-Origin': '*'}


@ebisu_api.route("/move_previous_station/<string:order_detail_id>")
class MoveToPreviousStation(Resource):

    @ebisu_api.marshal_with(order)
    def put(self, order_detail_id):
        order_details: OrderDetail = DbConnector.get_order_details_by_order_detail_id(order_detail_id)
        item = order_details.item

        station_sequence = find_station_sequence(item)
        next_station = station_sequence[station_sequence.index(order_details.station) - 1]
        order_details.station = next_station

        DbConnector.update_station_in_order_details(order_details)

        return None, 200, {'Access-Control-Allow-Origin': '*'}


@ebisu_api.route("/move_to_delivery_driver/<string:order_id>")
class MoveToDeliveryDriver(Resource):

    @ebisu_api.marshal_with(prioritized_order)
    def put(self, order_id):
        found_order: Order = None

        for order in DbConnector.get_orders():
            if order.id == order_id:
                found_order = order

        for order_detail in found_order.order_details:
            MoveToNextStation().put(order_detail.id)

        return None, 200, {'Access-Control-Allow-Origin': '*'}


@ebisu_api.route("/move_back_to_packer/<string:order_id>")
class MoveBackToPacker(Resource):

    @ebisu_api.marshal_with(prioritized_order)
    def put(self, order_id):
        found_order: Order = None

        for order in DbConnector.get_orders():
            if order.id == order_id:
                found_order = order

        for order_detail in found_order.order_details:
            MoveToPreviousStation().put(order_detail.id)

        return None, 200, {'Access-Control-Allow-Origin': '*'}


@ebisu_api.route("/mark_as_delivered/<string:order_id>")
class MarkAsDelivered(Resource):

    @ebisu_api.marshal_with(prioritized_order)
    def put(self, order_id):
        found_order: Order = None

        for order in DbConnector.get_orders():
            if order.id == order_id:
                found_order = order

        for order_detail in found_order.order_details:
            MoveToNextStation().put(order_detail.id)

        return None, 200, {'Access-Control-Allow-Origin': '*'}


@ebisu_api.route("/mark_as_onitsway/<string:order_id>")
class MarkAsOnItsWay(Resource):

    @ebisu_api.marshal_with(prioritized_order)
    def put(self, order_id):
        # TODO: Contact "on its way" notification endpoint from Just Eat Mock

        return None, 200, {'Access-Control-Allow-Origin': '*'}


def find_station_sequence(item: Item):
    if item.fried_roll:
        if item.fried_ingredient == "":
            return FRRI
        else:
            return FRFI
    else:
        if item.fried_ingredient == "":
            return NRRI
        else:
            return NRFI


if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0")
