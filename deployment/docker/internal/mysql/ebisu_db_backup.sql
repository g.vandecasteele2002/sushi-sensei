-- MySQL dump 10.13  Distrib 8.0.29, for Linux (x86_64)
--
-- Host: localhost    Database: ebisu
-- ------------------------------------------------------
-- Server version	8.0.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `delivery`
--

DROP TABLE IF EXISTS `delivery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `delivery` (
  `delivery_id` varchar(256) NOT NULL,
  `first_name` varchar(256) NOT NULL,
  `last_name` varchar(256) NOT NULL,
  `phone_number` varchar(256) DEFAULT NULL,
  `street` varchar(256) NOT NULL,
  `house_number` smallint NOT NULL,
  `postcode` smallint NOT NULL,
  PRIMARY KEY (`delivery_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `delivery`
--

LOCK TABLES `delivery` WRITE;
/*!40000 ALTER TABLE `delivery` DISABLE KEYS */;
INSERT INTO `delivery`
VALUES ('204c1a87-db69-11ec-8600-0242ac110002', 'John', 'Doe', '+49157123456', 'Imaginary str.', 69, 23089);
INSERT INTO `delivery`
VALUES ('0d1dbe12-ddb6-11ec-92fa-089798807c88', 'Albert', 'Danson', '+49157123456', 'Imaginary str.', 38, 45450);
INSERT INTO `delivery`
VALUES ('eaa60e43-ddb9-11ec-9b1e-089798807c88', 'Ben', 'Holligan', '+49157123456', 'Imaginary str.', 54, 35468);
/*!40000 ALTER TABLE `delivery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item` (
  `item_id` varchar(256) NOT NULL,
  `name` varchar(256) NOT NULL,
  `description` varchar(256) NOT NULL,
  `price` decimal(6,2) NOT NULL,
  `instructions` varchar(2048) DEFAULT NULL,
  `fried_roll` tinyint(1) NOT NULL,
  `fried_ingredient` varchar(64) DEFAULT NULL,
  `fried_ingredient_count` int DEFAULT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item`
--

LOCK TABLES `item` WRITE;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
INSERT INTO `item`
VALUES ('b88e704d-db68-11ec-8600-0242ac110002', 'Red Dragon Roll', 'Fiery Roll', 12.90, 'Make it good',
        0, NULL, NULL);
INSERT INTO `item`
VALUES ('2030743d-ddbb-11ec-8057-089798807c88', 'Mr Crabs Ura Maki Roll', 'Fiery Roll', 10.90,
        'Make it good',
        0, NULL, NULL);
INSERT INTO `item`
VALUES ('37ae3ff3-ddbb-11ec-94cf-089798807c88', 'Nigiri', 'Fiery Roll', 13.90, 'Make it good',
        1, 'Salmon, Flambeed Salmon, Tuna, Scampi, AvocadoSalmon, Flambeed Salmon, Tuna, Scampi, Avocado', 9);
INSERT INTO `item`
VALUES ('382c0478-ddbb-11ec-a606-089798807c88', 'Violet Roll Ura Maki Roll', 'Fiery Roll', 10.90,
        'Make it good',
        1, 'red beets, asparagus, cucumber, avocado', 4);
/*!40000 ALTER TABLE `item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_detail`
--

DROP TABLE IF EXISTS `order_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_detail` (
  `order_detail_id` varchar(256) NOT NULL,
  `order_id` varchar(256) NOT NULL,
  `quantity` int NOT NULL,
  `notes` varchar(256) DEFAULT NULL,
  `item_id` varchar(256) NOT NULL,
  `station` varchar(256) NOT NULL,
  PRIMARY KEY (`order_detail_id`,`order_id`),
  KEY `orderdetail_fk_item` (`item_id`),
  KEY `orderdetail_fk_order` (`order_id`),
  CONSTRAINT `orderdetail_fk_item` FOREIGN KEY (`item_id`) REFERENCES `item` (`item_id`),
  CONSTRAINT `orderdetail_fk_order` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_detail`
--

LOCK TABLES `order_detail` WRITE;
/*!40000 ALTER TABLE `order_detail` DISABLE KEYS */;
INSERT INTO `order_detail`
VALUES ('078c654a-db6c-11ec-8600-0242ac110002', '9555f17e-db69-11ec-8600-0242ac110002', 1, 'Make it super delicious',
        'b88e704d-db68-11ec-8600-0242ac110002', 'REGISTER');
INSERT INTO `order_detail`
VALUES ('85fcb459-ddc0-11ec-986c-089798807c88', '9555f17e-db69-11ec-8600-0242ac110002', 2, 'Make it super delicious',
        '2030743d-ddbb-11ec-8057-089798807c88', 'REGISTER');
INSERT INTO `order_detail`
VALUES ('8695bfcb-ddc0-11ec-833e-089798807c88', '9555f17e-db69-11ec-8600-0242ac110002', 1, 'Make it super delicious',
        '37ae3ff3-ddbb-11ec-94cf-089798807c88', 'REGISTER');
INSERT INTO `order_detail`
VALUES ('872ef2fd-ddc0-11ec-85c8-089798807c88', '1ce1b6f7-ddc1-11ec-8828-089798807c88', 1, 'Make it super delicious',
        '382c0478-ddbb-11ec-a606-089798807c88', 'PACKER');
INSERT INTO `order_detail`
VALUES ('87c7d731-ddc0-11ec-a3bd-089798807c88', '1ce1b6f7-ddc1-11ec-8828-089798807c88', 1, 'Make it super delicious',
        '2030743d-ddbb-11ec-8057-089798807c88', 'DELIVERY_DRIVER');
INSERT INTO `order_detail`
VALUES ('8861580f-ddc0-11ec-8451-089798807c88', '1ce1b6f7-ddc1-11ec-8828-089798807c88', 1, 'Make it super delicious',
        'b88e704d-db68-11ec-8600-0242ac110002', 'SUSHI_MAKER');
INSERT INTO `order_detail`
VALUES ('88fa15c1-ddc0-11ec-a393-089798807c88', '1d7c2222-ddc1-11ec-91d4-089798807c88', 1, 'Make it super delicious',
        '37ae3ff3-ddbb-11ec-94cf-089798807c88', 'FRYER');
INSERT INTO `order_detail`
VALUES ('89934863-ddc0-11ec-8df9-089798807c88', '1d7c2222-ddc1-11ec-91d4-089798807c88', 1, 'Make it super delicious',
        '382c0478-ddbb-11ec-a606-089798807c88', 'SUSHI_MAKER');

/*!40000 ALTER TABLE `order_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `orders` (
  `order_id` varchar(256) NOT NULL,
  `created_at` timestamp NOT NULL,
  `deliver_at` timestamp NULL DEFAULT NULL,
  `delivery_id` varchar(256) NOT NULL,
  `delivery_time_adjusted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_id`),
  KEY `order_fk_delivery` (`delivery_id`),
  CONSTRAINT `order_fk_delivery` FOREIGN KEY (`delivery_id`) REFERENCES `delivery` (`delivery_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders`
VALUES ('9555f17e-db69-11ec-8600-0242ac110002', '2022-05-24 15:57:00', '2022-05-24 16:30:00',
        '204c1a87-db69-11ec-8600-0242ac110002', 1);
INSERT INTO `orders`
VALUES ('1ce1b6f7-ddc1-11ec-8828-089798807c88', '2022-06-15 16:50:00', '2022-06-15 17:30:00',
        'eaa60e43-ddb9-11ec-9b1e-089798807c88', 1);
INSERT INTO `orders`
VALUES ('1d7c2222-ddc1-11ec-91d4-089798807c88', '2022-05-24 15:57:00', '2022-05-24 16:30:00',
        '0d1dbe12-ddb6-11ec-92fa-089798807c88', 0);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-05-30 21:47:44
