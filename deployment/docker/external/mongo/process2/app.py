from datetime import datetime
from pymongo import MongoClient
from flask import Flask, request, Response

app = Flask(__name__)

print("Establishing connection to database ....")
client = MongoClient("mongodb://localhost:27017")
db = client.justeat


@app.route('/')
def hello():
    return '<h1>Please make requests to the right endpoints</h1>'


@app.route('/orders/<order_id>/accept', methods=['PUT'])
def accept_order(order_id):
    result = db.order_containers.find({"order.Id": order_id}).limit(1)

    try:
        order = result.next()
    except:
        return Response("{'error':'order not found'}", status=404, mimetype='application/json')

    state = order["state"]


    if state == "not sent":
        return Response("{'error':'order not found'}", status=404, mimetype='application/json')
    elif state != "sent":

        return Response("{'conflict':'this order has either been accepted or rejected already'}", status=409, mimetype='application/json')
    else:
        # do the actual work

        #check the time
        try:
            time_accepted_for = request.json["TimeAcceptedFor"]
            my_date = datetime.strptime(time_accepted_for, "%Y-%m-%d %H:%M:%S")
        except:
            return Response("{'error':'request does not contain a valid time'}", status=400, mimetype='application/json')


        order["state"] = "accepted"
        order["order"]["deliver_at"] = time_accepted_for

        # write back to database
        res = db.order_containers.update_one({"_id": order["_id"]}, {"$set": order}, upsert=True)

    return Response("{'success':'acceptance of order confirmed'}", status=200,
                    mimetype='application/json')


@app.route('/orders/<order_id>/reject', methods=['PUT'])
def reject_order(order_id):
    result = db.order_containers.find({"order.Id": order_id}).limit(1)

    try:
        order = result.next()
    except:
        return Response("{'error':'order not found'}", status=404, mimetype='application/json')

    state = order["state"]

    if state == "not sent":
        return Response("{'error':'order not found'}", status=404, mimetype='application/json')
    elif state != "sent":

        return Response("{'conflict':'this order has either been accepted or rejected already'}", status=409,
                        mimetype='application/json')
    else:
        # do the actual work
        order["state"] = "rejected"

        # write back to database
        res = db.order_containers.update_one({"_id": order["_id"]}, {"$set": order}, upsert=True)

    return Response("{'success':'rejection of order confirmed'}", status=200,
                    mimetype='application/json')


@app.route('/orders/<order_id>/duedate', methods=['PUT'])
def update_duedate(order_id):
    result = db.order_containers.find({"order.Id": order_id}).limit(1)

    try:
        order = result.next()
    except:
        return Response("{'error':'order not found'}", status=404, mimetype='application/json')

    state = order["state"]
    due_date = ""

    if state == "not sent" or state == "rejected":
        return Response("{'error':'order not found'}", status=404, mimetype='application/json')
    else:
        try:
            due_date = request.json["DueDate"]
            my_date = datetime.strptime(due_date, "%Y-%m-%d %H:%M:%S")
        except:
            return Response("{'error':'request does not contain a valid duedate'}", status=400, mimetype='application/json')

    order["order"]["deliver_at"] = due_date
    res = db.order_containers.update_one({"_id": order["_id"]}, {"$set": order}, upsert=True)
    return Response("{'success':'duedate updated'}", status=200, mimetype='applicatio/json')


@app.route('/orders/<order_id>/deliverystate/onitsway', methods=['PUT'])
def update_order_onitsway(order_id):
    result = db.order_containers.find({"order.Id": order_id}).limit(1)

    try:
        order = result.next()
    except:
        return Response("{'error':'order not found'}", status=404, mimetype='application/json')

    state = order["state"]

    if state == "not sent":
        return Response("{'error':'order not found'}", status=404, mimetype='application/json')
    elif state != "accepted":

        return Response("{'conflict':'this order has not been accepted yet'}", status=409,
                        mimetype='application/json')
    else:
        # do the actual work
        order["state"] = "onitsway"

        # write back to database
        res = db.order_containers.update_one({"_id": order["_id"]}, {"$set": order}, upsert=True)

    return Response("{'success':'order state updated to onitsway'}", status=200,
                    mimetype='application/json')

@app.route('/orders/<order_id>/deliverystate/delivered', methods=['PUT'])
def update_order_delivered(order_id):
    result = db.order_containers.find({"order.Id": order_id}).limit(1)

    try:
        order = result.next()
    except:
        return Response("{'error':'order not found'}", status=404, mimetype='application/json')

    state = order["state"]

    if state == "not sent":
        return Response("{'error':'order not found'}", status=404, mimetype='application/json')
    elif state != "onitsway":

        return Response("{'conflict':'this order has not left the restaurant yet'}", status=409,
                        mimetype='application/json')
    else:
        # do the actual work
        order["state"] = "delivered"

        # write back to database
        res = db.order_containers.update_one({"_id": order["_id"]}, {"$set": order}, upsert=True)

    return Response("{'success':'order state updated to delivered'}", status=200,
                    mimetype='application/json')



