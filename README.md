# Sushi-Sensei Ebisu Semester Project


## MySQL Docker Container Tutorial
To create a backup of your local mysql database, you can use the following command:

```
docker exec ebisu_db /usr/bin/mysqldump -u root --password=ebisuforever123 ebisu > ebisu_db_backup.sql
```
Please keep in mind that this is going to overwrite the SQL dump that has been created to initiate the MySQL server from the Dockerfile build / the Docker compose script. If you want to do a backup separate of the version we all should be using, change the filename at the end of the command.

When setting up the MySQL Docker container, all you need to do is navigate to the location of the **docker-compose.yaml** file inside of the internal deployment folder. </br>
After that you can create the container by running:
```
docker-compose up
```
or
```
docker-compose down
```
to remove the container once you are done working with it. Keep in mind that none of the data is persisted, if you do not create a backup yourself, when you are starting the container the same way the next time.
